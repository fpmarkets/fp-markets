First Prudential Markets, commonly known as "FPMarkets" is an Australian-based award winning investment company providing over-the-counter (OTC) and exchange traded derivative products including direct market access (DMA) Contracts-For-Difference (CFDs), foreign exchange (Forex) and global futures.

Address: Level 5, 10 Bridge St, Sydney, NSW 2000, Australia 

Phone: +61 2 8252 6800
